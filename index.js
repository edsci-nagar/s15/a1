console.log('Hello, World')

const details = {
    firstName: 'Justin',
    lastName: 'Nagar',
    age: 19,
    hobbies : ['playing online games', 'lifitng weights', 'watch movies'] ,
    
    workAddress: {
        housenumber: 'Lot 581, Block Z',
        street: '40 pearl Street',
        city: 'Michigan City',
        state: 'United State',
    }
}
const work = Object.values(details.workAddress);

console.log("My First Name is " + details.firstName)

console.log("My Last Name is " + details.lastName)

console.log(`Yes, I am ${details.firstName} ${details.lastName}.`)

console.log("I am " + details.age + " years old.")

console.log(`My hobbies are ${details.hobbies.join(', ')}.`);

console.log("I work at " + work.join(", ") + ".");